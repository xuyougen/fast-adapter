package org.wavetech.fastadapter;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.wavetech.adapter.FastAdapter;
import org.wavetech.adapter.SpanSizeLookup;
import org.wavetech.adapter.ViewHolder;
import org.wavetech.adapter.AlphaItemAnimator;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final List<String> NAMES = Arrays.asList(
            "Charlie", "Milton", "Hilton", "Jane", "Sofia", "Nico", "Jackie", "Laurence", "Lily", "Jessica", "William",
            "Carl", "Tom", "Michelle", "Gatsby", "Linda", "Arthur", "Eliza", "Dan", "Bob", "Lucy", "Jimmy", "Nancy", "Daniel",
            "Taylor", "Bill", "Sofia", "Amy", "Anna", "Angela", "Adam", "Alexander", "Emily", "Joshua", "Sofia", "Michael",
            "Mike", "Drake", "Jones", "Peter", "Steve", "Victoria", "Jack", "Sofia", "Victor", "Newton", "Shakespeare",
            "Tesla", "Gauss", "Shelton", "Jackson", "Thomas", "Freeman", "Sebastian", "Jen", "Sofia", "Winston", "Abigail",
            "Shannon", "Elena"
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        FastAdapter<String> adapter = new FastAdapter<String>(this, R.layout.item_normal, NAMES) {

            @Override
            protected ViewHolder onCreateViewHolderImpl(@NonNull ViewGroup parent, int viewType) {
                if (Math.random() > 0.5) {
                    TextView textView = new TextView(parent.getContext());
                    textView.setId(R.id.title);
                    textView.setTextSize(40f);
                    //返回自定义的Holder
                    return new ViewHolder(textView);
                }
                //返回默认创建的Holder（需设置默认的layoutRes）
                return super.onCreateViewHolderImpl(parent, viewType);
            }

            //处理自定义的 itemType
            @Override
            protected int getItemViewTypeImpl(int position, int dataPosition) {
                return super.getItemViewTypeImpl(position, dataPosition);
            }

            @Override
            public void bindData(ViewHolder holder, int dataPosition, String s) {
                //绑定数据样式
                holder.setTextColorRes(R.id.title, R.color.design_default_color_primary_dark);
                holder.setText(R.id.title, s);
            }
        };
        //列表项点击
        adapter.setOnItemClickListener((view, holder, data) -> {
            Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
        });
        //列表项子视图点击事件处理
        adapter.setOnItemChildClickListener(R.id.trigger, (view, holder, data) -> {
            Toast.makeText(this, "trigger -> " + data, Toast.LENGTH_SHORT).show();
        });
        //设置动画, 可自定义实现ItemAnimator接口, once 为true时动画只执行一次
        adapter.setItemAnimator(new AlphaItemAnimator(), false);
        //设置自定义 SpanSizeLookup
        adapter.setSpanSizeLookup(new SpanSizeLookup<String>() {
            @Override
            public int getSpanSize(int itemViewType, int spanCount, int position, @Nullable String s) {
                //直接 return spanCount 代表此项占一整行
                if ("Sofia".equals(s)) return spanCount;
                return super.getSpanSize(itemViewType, spanCount, position, s);
            }
        });
        //添加 Header
        adapter.addHeaderView(makeATextView());
        //添加 Footer
        adapter.addFooterView(makeATextView());

        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setAdapter(adapter);

        findViewById(R.id.add).setOnClickListener(v -> {
            adapter.addHeaderView(makeATextView());
            recyclerView.scrollToPosition(0);
        });
        findViewById(R.id.remove).setOnClickListener(v -> adapter.removeHeaderView(0));
        findViewById(R.id.addFooter).setOnClickListener(v -> {
            adapter.addFooterView(makeATextView());
            recyclerView.scrollToPosition(adapter.getItemCount() - 1);
        });
        findViewById(R.id.removeFooter).setOnClickListener(v -> adapter.removeFooterView(0));
    }


    private TextView makeATextView() {
        TextView textView = new TextView(this);
        textView.setTextSize((float) (12f + Math.random() * 8f));
        textView.setTextColor(randomColor());
        textView.setGravity(Gravity.CENTER);
        textView.setText(NAMES.get((int) (Math.random() * NAMES.size())));
        textView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        return textView;
    }

    public static int randomColor() {
        return Color.rgb((int) (Math.random() * 256), (int) (Math.random() * 256), (int) (Math.random() * 256));
    }

}