# FastAdapter

#### 介绍
RecyclerView Adapter 常用功能(点击事件、绑定数据、Header、Footer、动画等)进行封装。

#### 截图
![FastAdapter](https://images.gitee.com/uploads/images/2021/0713/161317_b622306a_1555343.gif "3333.gif")

#### 使用说明

```
        FastAdapter<String> adapter = new FastAdapter<String>(this, R.layout.item_normal, NAMES) {
        
            @Override
            protected ViewHolder onCreateViewHolderImpl(@NonNull ViewGroup parent, int viewType) {
                if (Math.random() > 0.5) {
                    TextView textView = new TextView(parent.getContext());
                    textView.setId(R.id.title);
                    textView.setTextSize(40f);
                    //返回自定义的Holder
                    return new ViewHolder(textView);
                }
                //返回默认创建的Holder（需设置默认的layoutRes）
                return super.onCreateViewHolderImpl(parent, viewType);
            }
        
            //处理自定义的 itemType
            @Override
            protected int getItemViewTypeImpl(int position, int dataPosition) {
                return super.getItemViewTypeImpl(position, dataPosition);
            }

            @Override
            public void bindData(ViewHolder holder, String s) {
                //绑定数据样式
                holder.setTextColorRes(R.id.title, R.color.design_default_color_primary_dark);
                holder.setText(R.id.title, s);
            }
        };
        //列表项点击
        adapter.setOnItemClickListener((view, holder, data) -> {
            Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
        });
        //列表项子视图点击事件处理
        adapter.setOnItemChildClickListener(R.id.trigger, (view, holder, data) -> {
            Toast.makeText(this, "trigger -> " + data, Toast.LENGTH_SHORT).show();
        });
        //设置动画, 可自定义实现ItemAnimator接口, once 为true时动画只执行一次
        adapter.setItemAnimator(new AlphaItemAnimator(), false);
        //设置自定义 SpanSizeLookup
        adapter.setSpanSizeLookup(new SpanSizeLookup<String>() {
            @Override
            public int getSpanSize(int itemViewType, int spanCount, int position, @Nullable String s) {
                //直接 return spanCount 代表此项占一整行
                if ("Sofia".equals(s)) return spanCount;
                return super.getSpanSize(itemViewType, spanCount, position, s);
            }
        });
        //添加 Header
        adapter.addHeaderView(makeATextView());
        //添加 Footer
        adapter.addFooterView(makeATextView());

        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setAdapter(adapter);
```
