package org.wavetech.adapter;

import android.view.View;

public interface HeaderViewOwner {

    void addHeaderView(View view);

    void removeHeaderView(View view);

    void removeHeaderView(int position);
}
