package org.wavetech.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.wavetech.adapter.ViewHolder;

public interface OnItemClickListener<DATA> {
    void onItemClick(@NonNull View view, @NonNull ViewHolder holder, @Nullable DATA data);
}
