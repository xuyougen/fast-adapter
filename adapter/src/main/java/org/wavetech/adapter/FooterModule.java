package org.wavetech.adapter;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import org.wavetech.adapter.FooterViewOwner;

public class FooterModule implements FooterViewOwner {

    private final Context context;
    private RecyclerView.Adapter<?> adapter;
    private LinearLayout containerView;

    public FooterModule(Context context, RecyclerView.Adapter<?> adapter) {
        this.context = context;
        this.adapter = adapter;
    }

    public LinearLayout getContainerView() {
        ensureContainerExists();
        return containerView;
    }

    private void ensureContainerExists() {
        if (containerView == null) {
            containerView = new LinearLayout(context);
            containerView.setOrientation(LinearLayout.VERTICAL);
        }
    }

    public boolean hasFooterView() {
        return containerView != null && containerView.getChildCount() > 0;
    }

    @Override
    public void addFooterView(View view) {
        ensureContainerExists();
        boolean hasFooterView = hasFooterView();
        containerView.addView(view);
        if (!hasFooterView) {
            adapter.notifyItemInserted(adapter.getItemCount());
        }
    }

    @Override
    public void removeFooterView(View view) {
        if (!hasFooterView()) return;
        int index = containerView.indexOfChild(view);
        if (index < 0) return;
        containerView.removeView(view);
        if (!hasFooterView()) {
            adapter.notifyItemRemoved(adapter.getItemCount());
        }
    }

    @Override
    public void removeFooterView(int position) {
        if (!hasFooterView()) return;
        if (position > containerView.getChildCount() - 1) return;
        containerView.removeViewAt(position);
        if (!hasFooterView()) {
            adapter.notifyItemRemoved(adapter.getItemCount());
        }
    }


}
