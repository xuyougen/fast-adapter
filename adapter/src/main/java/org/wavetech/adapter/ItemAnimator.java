package org.wavetech.adapter;

import android.view.View;

import androidx.annotation.NonNull;

public interface ItemAnimator {

    void animate(@NonNull View view, boolean enter);

}
