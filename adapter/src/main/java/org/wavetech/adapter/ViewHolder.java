package org.wavetech.adapter;

import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.HashMap;

public class ViewHolder extends RecyclerView.ViewHolder {

    private final SparseArray<View> viewReferences;
    private final HashMap<String, Object> tagMap;

    {
        viewReferences = new SparseArray<>();
        tagMap = new HashMap<>();
    }

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public ViewHolder(@NonNull ViewGroup parent, @LayoutRes int layout) {
        this(LayoutInflater.from(parent.getContext()).inflate(layout, parent, false));
    }

    @SuppressWarnings("unchecked cast")
    public <T extends View> T findViewById(@IdRes int id) {
        View view = viewReferences.get(id);
        if (view == null) {
            view = itemView.findViewById(id);
            viewReferences.put(id, view);
        }
        return (T) view;
    }

    public ViewHolder setTag(String key, Object tag) {
        this.tagMap.put(key, tag);
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> T getTag(String key) {
        return (T) tagMap.get(key);
    }

    /* ----------- BASIC PROPERTIES START ----------- */
    public ViewHolder setBackground(@IdRes int id, Drawable drawable) {
        findViewById(id).setBackground(drawable);
        return this;
    }

    public ViewHolder setBackground(@IdRes int id, @DrawableRes int resId) {
        findViewById(id).setBackgroundResource(resId);
        return this;
    }

    public void setBackgroundColor(@IdRes int id, @ColorInt int color) {
        findViewById(id).setBackgroundColor(color);
    }

    public ViewHolder setVisibility(@IdRes int id, int visibility) {
        findViewById(id).setVisibility(visibility);
        return this;
    }

    public ViewHolder setEnabled(@IdRes int id, boolean enabled) {
        findViewById(id).setEnabled(enabled);
        return this;
    }
    /* ----------- BASIC PROPERTIES END ----------- */


    /* ----------- TEXT PROPERTIES START ----------- */
    public ViewHolder setText(@IdRes int id, CharSequence sequence) {
        ((TextView) findViewById(id)).setText(sequence);
        return this;
    }

    public ViewHolder setHintText(@IdRes int id, CharSequence sequence) {
        ((TextView) findViewById(id)).setHint(sequence);
        return this;
    }

    public ViewHolder setHtmlText(@IdRes int id, String html) {
        ((TextView) findViewById(id)).setText(Html.fromHtml(html));
        return this;
    }

    public ViewHolder setTextSize(@IdRes int id, float size) {
        ((TextView) findViewById(id)).setTextSize(size);
        return this;
    }

    public ViewHolder setTextColor(@IdRes int id, @ColorInt int color) {
        ((TextView) findViewById(id)).setTextColor(color);
        return this;
    }

    public ViewHolder setTextColorRes(@IdRes int id, @ColorRes int color) {
        TextView textView = findViewById(id);
        textView.setTextColor(Utils.getColor(textView.getContext(), color));
        return this;
    }

    public ViewHolder setHintTextColor(@IdRes int id, @ColorInt int color) {
        ((TextView) findViewById(id)).setHintTextColor(color);
        return this;
    }

    public ViewHolder setHintTextColorRes(@IdRes int id, @ColorRes int color) {
        TextView textView = findViewById(id);
        textView.setHintTextColor(Utils.getColor(textView.getContext(), color));
        return this;
    }
    /* ----------- TEXT PROPERTIES END ----------- */

    /* ----------- IMAGE PROPERTIES START ----------- */
    public ViewHolder setImageResource(@IdRes int id, int resId) {
        ((ImageView) findViewById(id)).setImageResource(resId);
        return this;
    }

    public ViewHolder setImageDrawable(@IdRes int id, Drawable drawable) {
        ((ImageView) findViewById(id)).setImageDrawable(drawable);
        return this;
    }
    /* ----------- IMAGE PROPERTIES END ----------- */

}
