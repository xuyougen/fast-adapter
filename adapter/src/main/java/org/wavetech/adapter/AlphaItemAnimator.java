package org.wavetech.adapter;

import android.view.View;

import androidx.annotation.NonNull;

import org.wavetech.adapter.ItemAnimator;

public class AlphaItemAnimator implements ItemAnimator {
    @Override
    public void animate(@NonNull View view, boolean enter) {
        view.setAlpha(0f);
        view.animate().alpha(1f).start();
    }
}
