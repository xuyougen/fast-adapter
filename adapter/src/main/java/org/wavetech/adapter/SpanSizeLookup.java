package org.wavetech.adapter;

import androidx.annotation.Nullable;

public class SpanSizeLookup<DATA> {

    @NoOverride
    int getSpanSize(int itemViewType, int position, @Nullable DATA data, int spanCount, boolean headerAsFlow, boolean footerAsFlow) {
        if (itemViewType == FastAdapter.ITEM_HEADER) {
            if (headerAsFlow) {
                return 1;
            } else {
                return spanCount;
            }
        }
        if (itemViewType == FastAdapter.ITEM_FOOTER) {
            if (footerAsFlow) {
                return 1;
            } else {
                return spanCount;
            }
        }
        return getSpanSize(itemViewType, spanCount, position, data);
    }

    /**
     * 要设置自定义 span 跨度大小 重写此方法即可
     *
     * @param itemViewType item类型,和adapter的类型中对应
     * @param spanCount    一行总共的列数
     * @param position     位置
     * @param data         当前数据
     * @return 当前列表项span大小
     */
    public int getSpanSize(int itemViewType, int spanCount, int position, @Nullable DATA data) {
        return 1;
    }

}
