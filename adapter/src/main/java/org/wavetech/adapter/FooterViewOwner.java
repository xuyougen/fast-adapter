package org.wavetech.adapter;

import android.view.View;

public interface FooterViewOwner {

    void addFooterView(View view);

    void removeFooterView(View view);

    void removeFooterView(int position);
}
